<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$id = get('id');
if (empty($id)) {
    redirect("/admin/movies/list.php");
}

$page_path = "/admin/movies/edit.php?id={$id}";

if (post()) {
    
    if (!empty($_FILES['poster']['name'])) {
        $poster = upload('poster', '/storage/posters');
        if (!$poster) {
            setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพโปสเตอร์ภาพยนตร์ได้");
            redirect($page_path);
        }
        
        $db->query("UPDATE `movies` SET `poster`='{$poster}' WHERE `movie_id`='{$id}'");
    }

    $qr = $db->query("UPDATE `movies` SET `name`='{$_POST['name']}' WHERE `movie_id`='{$id}'");
    if ($qr) {
        setAlert('success', "แก้ไขภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขภาพยนตร์ได้");
    }

    redirect($page_path);
}

$data = db_row("SELECT * FROM `movies` WHERE `movie_id`='{$id}'");
if (empty($data)) {
    redirect("/admin/movies/list.php");
}

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="poster">
        <img src="<?= url($data['poster']) ?>" alt="" style="
            max-width: 22rem;
        ">
    </label>
    <br>
    <label for="poster">ภาพโปสเตอร์ภาพยนตร์</label>
    <input type="file" name="poster" id="poster" accept="image/*">
    <br>
    <label for="name">ชื่อภาพยนตร์</label>
    <input type="text" name="name" id="name" value="<?= $data['name'] ?>" required>
    <br>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขภาพยนตร์';
require ROOT . '/admin/layout.php';
