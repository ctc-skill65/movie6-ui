<!DOCTYPE html>
<html lang="th">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= isset($page_name) ? $page_name . ' -' : null ?> ระบบสำรองที่นั่งโรงภาพยนตร์</title>
    <?= isset($layout_head) ? $layout_head : null ?>
</head>
<body>
    <?= isset($layout_body) ? $layout_body : null ?>

    <script>
        window.onload = function() {
            setTimeout(() => {
                window.dispatchEvent(new CustomEvent('pageLoad'))
            }, 100);
        }
    </script>
</body>
</html>
